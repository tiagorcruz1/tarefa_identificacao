# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 19:28:16 2019

@author: tiago
"""

import numpy as np
from numpy.linalg import inv # inverter matrizes
# lfilter(b, a, x)	Filter data along one-dimension with an IIR or FIR filter.
from scipy import signal
from math import sqrt
from sklearn.metrics import mean_squared_error


def identificacaoPred(u,y,N):
    # Cria a phi
    phi = np.zeros((N, 2))
    
    for i in range(1,N):
        phi[i,0] = u[i-1]
        phi[i,1] = -y[i-1]
        
    # encontra theta
    theta_chapeu = phi.T@phi
    theta_chapeu = inv(theta_chapeu)
    theta_chapeu = theta_chapeu@phi.T
    theta_chapeu = theta_chapeu@y
    
    num = [0, theta_chapeu[0]]
    den = [1, theta_chapeu[1]]
    
    # simulacao
    y_sim = signal.lfilter(num, den, u)
    
    # predicao
    y_pred = signal.lfilter(num,1,u) - signal.lfilter(den,1,y) + y
    
    rms_p = sqrt(mean_squared_error(y, y_pred)) # predicao
    rms_s = sqrt(mean_squared_error(y, y_sim)) # simulacao
    
    return(y_sim, y_pred, rms_s, rms_p, theta_chapeu)