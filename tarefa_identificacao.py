# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 09:14:13 2019

@author: UFRGS
"""

# importa modulos
import numpy as np
# lfilter(b, a, x)	Filter data along one-dimension with an IIR or FIR filter.
from scipy import signal
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset # efeito zoom (caixa)
import funcoes

# fecha figuras
plt.close("all")

# variaveis do sistema
h = 0.001
m = 0.01
B = 0.1

# F.T.
b = h/m
a = 1 - h*B/m

# entrada
N = 1000 # numero de amostras
u = np.zeros(N)
u[200] = 10 # f = 10

# referencia de amostras 
k = np.arange(N)

# saida
y_ideal = signal.lfilter([0, b],[1, -a], u)

# ruido branco
ruido = np.random.randn(N)/100

# ruido para sistema ARX
ruido_filtrado = signal.lfilter([1], [1, -a],  ruido)

# saida para ruido branco
y_r1 = y_ideal + ruido

# saida para ruido filtrado
y_r2 = y_ideal + ruido_filtrado

# identificacao
[y_sim, y_pred, rms_s, rms_p, theta_chapeu] = funcoes.identificacaoPred(u,y_r1,N)
print("\nRMS: ", rms_p)
print("theta_chapeu: ", theta_chapeu, "\n")

# -----------------------------------------------------------------------------
# ruido branco
# -----------------------------------------------------------------------------

# plot
fig, ax = plt.subplots() # create a new figure with a default 111 subplot
ax.plot(k,y_ideal,"b.")
ax.plot(k, y_r1,"k.")
ax.plot(k,y_sim,"y,")
ax.plot(k,y_pred,"r.")
ax.set_xlabel("Amostra"), ax.set_ylabel("Velocidade [m/s]")
ax.set_title("Ruído Branco")
axins = zoomed_inset_axes(ax, 12, loc='upper right') # zoom-factor: 2.5, location: upper-left
axins.plot(k,y_ideal,"b--")
axins.plot(k, y_r1,"k--.")
axins.plot(k,y_sim,"y--.")
axins.plot(k,y_pred,"r--.")
axins.legend(('Ideal','Medição', 'Simulação', 'Predição'), loc='upper right')
x1, x2, y1, y2 = 290, 330, 0.3, 0.36 # specify the limits
axins.set_xlim(x1, x2) # apply the x-limits
axins.set_ylim(y1, y2) # apply the y-limits
plt.yticks(visible=False)
plt.xticks(visible=False)
mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

# -----------------------------------------------------------------------------
# ruido filtrado
# -----------------------------------------------------------------------------

[y_sim, y_pred, rms_s, rms_p, theta_chapeu] = funcoes.identificacaoPred(u,y_r2,N)
print("\nRMS: ", rms_p)
print("theta_chapeu: ", theta_chapeu, "\n")

# plot
fig, ax = plt.subplots() # create a new figure with a default 111 subplot
ax.plot(k,y_ideal,"b.")
ax.plot(k, y_r2,"k.")
ax.plot(k,y_sim,"y,")
ax.plot(k,y_pred,"r.")
ax.set_xlabel("Amostra"), ax.set_ylabel("Velocidade [m/s]")
ax.set_title("Ruído Filtrado")
axins = zoomed_inset_axes(ax, 12, loc='upper right') # zoom-factor: 2.5, location: upper-left
axins.plot(k,y_ideal,"b--")
axins.plot(k, y_r2,"k--.")
axins.plot(k,y_sim,"y--.")
axins.plot(k,y_pred,"r--.")
axins.legend(('Ideal','Medição', 'Simulação', 'Predição'), loc='upper right')
x1, x2, y1, y2 = 290, 330, 0.3, 0.36 # specify the limits
axins.set_xlim(x1, x2) # apply the x-limits
axins.set_ylim(y1, y2) # apply the y-limits
plt.yticks(visible=False)
plt.xticks(visible=False)
mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

# -----------------------------------------------------------------------------
# analise intensidade de ruido
# -----------------------------------------------------------------------------

theta_0 = []
theta_1 = []
rms_s_aux = []
rms_p_aux = []
# ciclo para análise de intensidade de ruído
intensidade = 10
for i in range(1,4):
    ruido = np.random.randn(N)/(intensidade)
    intensidade = intensidade * 5
    ruido_filtrado = signal.lfilter([1], [1, -a],  ruido)
    y_r3 = y_ideal + ruido
    y_r4 = y_ideal + ruido_filtrado
    [y_sim, y_pred, rms_s, rms_p, theta_chapeu] = funcoes.identificacaoPred(u,y_r3,N)
    theta_0.append(theta_chapeu[0])
    theta_1.append(theta_chapeu[1])
    rms_s_aux.append(rms_s)
    rms_p_aux.append(rms_p)
    [y_sim, y_pred, rms_s, rms_p, theta_chapeu] = funcoes.identificacaoPred(u,y_r4,N)
    theta_0.append(theta_chapeu[0])
    theta_1.append(theta_chapeu[1])
    rms_s_aux.append(rms_s)
    rms_p_aux.append(rms_p)
    
# -----------------------------------------------------------------------------
# analise de dispersao
# -----------------------------------------------------------------------------

# numero de repeticoes
M = 100

# matriz para armazenar parametros
# colunas 1 e 2 thetas do sistema com ruido branco
# colunas 3 e 4 thetas do sistema com ruido filtrado
theta_disp = np.zeros((M, 4))

# intensidade de ruido
ruido_int = 1/10
ruido_int = 1/100
ruido_int = 1/1000

for j in range(0, M):
    ruido = np.random.randn(N)*ruido_int
    ruido_filtrado = signal.lfilter([1], [1, -a],  ruido)
    y_r5 = y_ideal + ruido
    y_r6 = y_ideal + ruido_filtrado
    [y_sim, y_pred, rms_s, rms_p, theta_chapeu] = funcoes.identificacaoPred(u,y_r5,N)
    theta_disp[j,0] = theta_chapeu[0]
    theta_disp[j,1] = theta_chapeu[1]
    [y_sim, y_pred, rms_s, rms_p, theta_chapeu] = funcoes.identificacaoPred(u,y_r6,N)
    theta_disp[j,2] = theta_chapeu[0]
    theta_disp[j,3] = theta_chapeu[1]

# plot
plt.figure(3)
plt.subplot(2,1,1)
plt.plot(theta_disp[:,0], theta_disp[:,1], ".")
plt.plot(0.1, -0.99, "ro") # referencia
plt.title("Análise de dispersão - Ruído branco * " + str(ruido_int))
plt.subplot(2,1,2)
plt.plot(theta_disp[:,2], theta_disp[:,3], ".")
plt.plot(0.1, -0.99, "ro") # referencia
plt.title("Análise de dispersão - Ruído filtrado")




# plot

#plt.figure(1)
#plt.plot(k,y_ideal,"b.")
#plt.plot(k,y,"k.")
#plt.plot(k,y_sim,"y.")
#plt.plot(k,y_pred,"r.")
#plt.legend(('Ideal', 'Medição', 'Simulação', 'Predição'), loc='upper right')
##plt.plot(k,u)

plt.figure(2)
plt.plot(k,y_ideal,".")
plt.title("Resposta ao impulso")
plt.xlabel("Amostra"), plt.ylabel("Velocidade [m/s]")
